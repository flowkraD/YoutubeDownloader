# Youtube Downloader

Ever liked a YouTube video so much that you wanted to download it only to find out that the sites you went to spring up with non stop popups and you feel like punching your monitor?
<br />
<br />
Well that ends now. [Youtube Downloader](https://github.com/JavaSkreeet/YoutubeDownloader) is an open source solution to your needs. You have the control over the code and what it does. Highly efficient with an additional aspect of customizability - change the aspects you don't like, improve the aspects that aren't as efficient.

### This is what it looks like

![alt text](https://github.com/JavaSkreeet/YoutubeDownloader/blob/master/Screenshot/Screenshot.jpeg)
<br />
<br />
I know it doesn't look pretty, but it get the job done.

### Dependecies

[Electron](https://www.npmjs.com/package/electron) - Framework that made this all possible
<br />
[YTDL-core](https://www.npmjs.com/package/ytdl-core) - Used to fetch video ID and video itself
<br />
[YTDL-getinfo](https://www.npmjs.com/package/ytdl-getinfo) - fetches the video information like title

### Contributions

Feedbacks and Codebacks are highly appreciated.
Thank you for taking your time to read this.

### Packaging is still underway.
