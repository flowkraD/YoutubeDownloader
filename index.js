const electron = require("electron");           //Converts webpage into desktop app
const fs = require('fs');                       //FS for saving the video
const url = require("url");                     //Fetching and manipullation of URL
const path = require("path");                   //Joining file path 
const ytdl = require("ytdl-core");              //Fetching video stream
const fetchVideoInfo = require("youtube-info"); //Fetching video info

const{app,BrowserWindow,Menu,ipcMain}=electron; //Getting required Electron components
process.env.NODE_ENV = "devlopment";            //Helps eo toggle DEV Console
let mainWindow;                                 //Main app window

app.on("ready",function(){
    Creating the mainwindow
    mainWindow = new BrowserWindow({});
    //Building from HTML document
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname,"mainWindow.html"),
        protocol: "file:",
        slashes: true
    }));
    //Quiting the app when main window closes
    mainWindow.on("closed",function(){
        app.quit();
    });
    //Building the Menu Bar from the template
    const mainMenu = Menu.buildFromTemplate(mmt);
    Menu.setApplicationMenu(mainMenu);
});


//Catch Item from one window
ipcMain.on("item:add",function(e,item){
    //Getting the video ID for the given URL
    var id = ytdl.getURLVideoID(item);
    //Checking for the best quality
    ytdl.getInfo(item,{quality:"highest"}, function(err, info){
        var qua="137";
        console.log(info.formats[0].resolution);
        if(info.formats[0].resolution=="720p"||info.formats[0].resolution=="480p"||info.formats[0].resolution=="360p"||info.formats[0].resolution=="240p"||info.formats[0].resolution=="144p"){
                qua="highest";   
        }
        //Getting information and saving the multimedia stream
        fetchVideoInfo(`${id}`).then(function (videoInfo) {    
            ytdl(item, { filter: (format) => format.container === 'mp4',quality:qua}).pipe(fs.createWriteStream(`${__dirname}/Downloads/${videoInfo.title}.mp4`));
        });      
    });
});

//Menu Template
const mmt = [
    {
        label: "File",
        submenu:[
            {
                label: "Clear Download History",
                click(){
                    mainWindow.webContents.send("item:clear");//Sending message to mainwindow 
                }
            },
            {
                label:"Quit",
                accelerator: process.platform == "darwin" ? "Command+Q" : "Ctrl+Q", //for OS specific shortcuts
                click(){
                    app.quit();
                }
            }
        ]
    }
];

if(process.platform == "darwin"){
    mmt.unshift({});//works like pushing element into a stack to put it on top
}
//If the above is not done, OSX shows electron icon in the menu bar

if(process.env.NODE_ENV != "production"){
    mmt.push({//Toggling Dev tools
        label: "Developer Tools",
        submenu:[
            {   
                label:"Toggle Dev Tools",
                accelerator: process.platform == "darwin" ? "Command+I" : "Ctrl+I",
                lable:"Toggle DevTools",
                click(item,focusedWindow){
                    focusedWindow.toggleDevTools();
                },
            },
            {
                role: "reload"//Reloading after a change
            }
        ]
    });
}
